package com.nfs.fifth;

import com.nfs.fifth.exception.DateNotFoundException;
import com.nfs.fifth.model.BilanMois;
import com.nfs.fifth.model.Client;
import com.nfs.fifth.dto.CreateTout;
import com.nfs.fifth.model.Facture;
import com.nfs.fifth.model.Formation;
import com.nfs.fifth.service.BilanMoisService;
import com.nfs.fifth.service.ClientService;
import com.nfs.fifth.service.FactureService;
import com.nfs.fifth.service.FormationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    FormationService formationService;

    @Autowired
    FactureService factureService;

    @Autowired
    ClientService clientService;

    @Autowired
    BilanMoisService bilanMoisService;

    @GetMapping("/facturemois")
    public String facturedumois(@RequestParam(name= "month") int month, @RequestParam(name= "year") int year , Model model) {

        List<Facture> factures = bilanMoisService.getFactureFromMois(month, year);
        List<Formation> formations = formationService.getFormationMois(factures);
        List<Client> clients = clientService.getClientMois(formations);



        model.addAttribute("bilan_month", month);
        model.addAttribute("bilan_year", year);

        model.addAttribute("factures", factures);
        model.addAttribute("formations", formations);
        model.addAttribute("clients", clients);
        model.addAttribute("nb_total_candidat", formationService.getAllNbCandidat(factures));
        model.addAttribute("nb_total_formation", formationService.getNbFormation(factures));
        model.addAttribute("total_ttc", factureService.getTotalTTC(factures));
        model.addAttribute("total20", factureService.getTotalTva20(factures));
        model.addAttribute("total10", factureService.getTotalTva10(factures));
        model.addAttribute("total5", factureService.getTotalTva5(factures));
        model.addAttribute("total2", factureService.getTotalTva2(factures));
        model.addAttribute("total0", factureService.getTotalTva0(factures));
        model.addAttribute("soldemois", factureService.soldeMois(factures));
        model.addAttribute("total_tva", factureService.getTotalTva(factures));
        model.addAttribute("total_ht", factureService.getTotalHT(factures));


        return "facturedumois";
    }

    @GetMapping("/create")
    public String formulaire(Model model){
        model.addAttribute("createTout", new CreateTout());
        return "formulaire";
    }

    @PostMapping("/create")
    public String formulaireSubmit(@ModelAttribute(value = "client_name") String client_name,
                         @ModelAttribute(value = "action") String action,
                         @ModelAttribute(value = "nb_candidat") int nb_candidat,
                         @ModelAttribute(value = "date_formation") String date_formation,
                         @ModelAttribute(value = "month") int month,
                         @ModelAttribute(value = "year") int year,
                         @ModelAttribute(value = "price") double price,
                         @ModelAttribute(value = "tva") double tva,
                         @ModelAttribute(value = "date_facture") String date_facture,
                         @ModelAttribute(value = "type") String type,
                         @ModelAttribute(value = "ref") String ref,
                         @ModelAttribute(value = "nature") String nature,  Model model) {


        LocalDate dateBilan = LocalDate.of(year,month,1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


            LocalDate dateFormation = LocalDate.parse(date_formation, formatter);
            LocalDate dateFacture = LocalDate.parse(date_facture, formatter);


        Client client = clientService.createOrReturnExistingClient(client_name);
        Formation formation = formationService.createFormation(action,nb_candidat, dateFormation, client);
        BilanMois bilanMois = bilanMoisService.createBilanMois(dateBilan);
        Facture facture = factureService.createFacture(price, tva, type, dateFacture, ref, nature, bilanMois, formation);
        formationService.setFacture(formation, facture);
        System.out.println("le bilal : "+bilanMois);
        System.out.println("le factur : "+facture);
        bilanMoisService.addFacture(bilanMois, facture);

        return "formulaireOk";
    }



}
