package com.nfs.fifth;

import com.nfs.fifth.repository.BilanMoisRepository;
import com.nfs.fifth.repository.ClientRepository;
import com.nfs.fifth.repository.FactureRepository;
import com.nfs.fifth.repository.FormationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class FifthApplication {

	public static void main(String[] args) {

		SpringApplication.run(FifthApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(ClientRepository clientRepository,
								  FactureRepository factureRepository,
								  FormationRepository formationRepository,
								  BilanMoisRepository bilanMoisRepository){
		return (args) -> {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 2021);
			cal.set(Calendar.MONTH, Calendar.JANUARY);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			Date dateRepresentation = cal.getTime();

			//clientRepository.save(new Client("Jacques"));
			//formationRepository.save(new Formation("Prestation",12, dateRepresentation, clientRepository.findAll().get(0)));
			//bilanMoisRepository.save(new BilanMois( dateRepresentation));
			//factureRepository.save(new Facture(255.54, 20,dateRepresentation , "Prestation Java JEE",  formationRepository.findAll().get(0), bilanMoisRepository.findAll().get(0)));
			//formationRepository.findAll().get(0).setFacture(factureRepository.findAll().get(0));
			//factureRepository.findAll().get(0).setBilanmois(bilanMoisRepository.findAll().get(0));






			//userService.register("email6","password6");
		};

	}
}
