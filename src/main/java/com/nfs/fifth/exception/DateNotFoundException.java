package com.nfs.fifth.exception;

public class DateNotFoundException extends Exception{
    public DateNotFoundException(int month, int year) {
        super("Could not find date " + month + "/" + year);
    }
}
