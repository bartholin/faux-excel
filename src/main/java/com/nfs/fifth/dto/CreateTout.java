package com.nfs.fifth.dto;

import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.LocalDate;
import java.util.Date;

public class CreateTout {
    private String client_name;
    private String action;
    private int nb_candidat;
    private String date_formation;
    private int month;
    private int year;
    private double price;
    private double tva;
    private String date_facture;
    private String type;
    private String ref;
    private String nature;

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getNb_candidat() {
        return nb_candidat;
    }

    public void setNb_candidat(int nb_candidat) {
        this.nb_candidat = nb_candidat;
    }

    public String getDate_formation() {
        return date_formation;
    }

    public void setDate_formation(String date_formation) {
        this.date_formation = date_formation;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public String getDate_facture() {
        return date_facture;
    }

    public void setDate_facture(String date_facture) {
        this.date_facture = date_facture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }
}
