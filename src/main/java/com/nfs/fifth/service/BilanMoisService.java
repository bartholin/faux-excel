package com.nfs.fifth.service;

import com.nfs.fifth.model.BilanMois;
import com.nfs.fifth.model.Facture;
import com.nfs.fifth.repository.BilanMoisRepository;
import com.nfs.fifth.repository.FactureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class BilanMoisService {

    @Autowired
    BilanMoisRepository bilanMoisRepository;

    @Autowired
    FactureRepository factureRepository;

    public BilanMois createBilanMois(LocalDate dateBilan){
        if(bilanMoisRepository.findByDate(dateBilan)!=null){
            return bilanMoisRepository.findByDate(dateBilan);
        }
        else {
            return bilanMoisRepository.save(new BilanMois(dateBilan));
        }
    }

    public void addFacture(BilanMois bilanMois,Facture facture){
        bilanMois.addFacture(facture);
    }

    public List<Facture> getFactureFromMois(int month, int year){
        List<BilanMois> BilanMoisEns = bilanMoisRepository.findAll();
        LocalDate date ;
        BilanMois moisActuel=null;

        for (BilanMois bilanmois:BilanMoisEns) {

            date = bilanmois.getDateBilan();

            if(date.getMonthValue()==month&& date.getYear()==year){
                moisActuel = bilanmois;
            }
        }

        List<Facture> factures = factureRepository.findAll();
        BilanMois MoisActuel = moisActuel;
        ArrayList<Facture> facturesMois = new ArrayList<>(Collections.emptyList());

        for (Facture facture:factures){
            if(MoisActuel !=null&& MoisActuel.getId()==facture.getBilanmois().getId()){
                facturesMois.add(facture);
            }
        }

        return facturesMois;



    }
}
