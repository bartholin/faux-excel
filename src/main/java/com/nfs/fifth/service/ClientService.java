package com.nfs.fifth.service;

import com.nfs.fifth.model.Client;
import com.nfs.fifth.model.Formation;
import com.nfs.fifth.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public List<Client> getClientMois(List<Formation> formations){

        ArrayList<Client> client = new ArrayList<>();

        for (Formation formation:formations) {
            client.add(formation.getClient());
        }
        return client;
    }

    public Client createOrReturnExistingClient(String name){
        if(clientRepository.findByName(name)!=null){
            return clientRepository.findByName(name);
        }else{
            return clientRepository.save(new Client(name));
        }
    }
}
