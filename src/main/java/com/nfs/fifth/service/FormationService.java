package com.nfs.fifth.service;

import com.nfs.fifth.model.Client;
import com.nfs.fifth.model.Facture;
import com.nfs.fifth.model.Formation;
import com.nfs.fifth.repository.FormationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class FormationService {

    @Autowired
    FormationRepository repository;

    public void setFacture(Formation formation, Facture facture){
        repository.findById(formation.getId()).setFacture(facture);
    }

    public Formation createFormation(String action, int nbCandidat, LocalDate dateFormation, Client client){
        return repository.save(new Formation(action, nbCandidat, dateFormation, client));
    }

    public int getAllNbCandidat(List<Facture> factures){
        int total=0;
        List<Formation> formations = new ArrayList<Formation>(Collections.emptyList());
        for (Facture facture:factures) {
            formations.add(facture.getFormation());
        }

        for (Formation formation:formations) {
            total+=formation.getNbCandidat();
        }
        return total;
    }

    public int getNbFormation(List<Facture> factures){

        List<Formation> formations = new ArrayList<Formation>(Collections.emptyList());
        for (Facture facture:factures) {
            formations.add(facture.getFormation());
        }
        return formations.size();
    }



    public List<Formation> getFormationMois(List<Facture> factures){

        ArrayList<Formation> formations = new ArrayList<>();

        for (Facture facture:factures) {
            formations.add(facture.getFormation());
        }
        return formations;
    }
}
