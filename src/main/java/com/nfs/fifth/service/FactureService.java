package com.nfs.fifth.service;

import com.nfs.fifth.model.BilanMois;
import com.nfs.fifth.model.Facture;
import com.nfs.fifth.model.Formation;
import com.nfs.fifth.repository.FactureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class FactureService {

    @Autowired
    FactureRepository factureRepository;

    public Facture createFacture(double price, double tva, String type, LocalDate dateFacture, String ref, String nature, BilanMois bilanMois, Formation formation){
        return factureRepository.save(new Facture(price, tva, type,dateFacture , ref,nature, bilanMois, formation));
    }

    public double getTotalHT(List<Facture> factures){
        double totalHT = 0.0;
        for (Facture facture:factures) {
            totalHT+=facture.getPrice();
        }
        return totalHT;
    }

    public double getTotalTva20(List<Facture> factures){
        double nbTva20 = 0.0;
        for (Facture facture:factures) {
            if(facture.getTva()==20.0){
                nbTva20+=facture.getTvaSomme();
            }
        }
        return nbTva20;
    }

    public double getTotalTva10(List<Facture> factures){
        double nbTva10 = 0.0;
        for (Facture facture:factures) {
            if(facture.getTva()==10.0){
                nbTva10+=facture.getTvaSomme();
            }
        }
        return nbTva10;
    }
    public double getTotalTva5(List<Facture> factures){
        double nbTva5 = 0.0;
        for (Facture facture:factures) {
            if(facture.getTva()==5.5){
                nbTva5+=facture.getTvaSomme();
            }
        }
        return nbTva5;
    }
    public double getTotalTva2(List<Facture> factures){
        double nbTva2 = 0.0;
        for (Facture facture:factures) {
            if(facture.getTva()==2.0){
                nbTva2+=facture.getTvaSomme();
            }
        }
        return nbTva2;
    }
    public double getTotalTva0(List<Facture> factures){
        double nbTva0 = 0.0;
        for (Facture facture:factures) {
            if(facture.getTva()==0.0){
                nbTva0+=facture.getTvaSomme();
            }
        }
        return nbTva0;
    }

    public double soldeMois(List<Facture> factures){
        double soldeTotal = 0.0;
        for (Facture facture:factures) {
            soldeTotal+=facture.getPriceTTC();
        }
        return soldeTotal;
    }

    public double getTotalTva(List<Facture> factures){
        return getTotalTva0(factures)+getTotalTva2(factures)+getTotalTva5(factures)+getTotalTva10(factures)+getTotalTva20(factures);
    }

    public double getTotalTTC(List<Facture> factures){
        return getTotalHT(factures)-getTotalTva(factures);
    }
}
