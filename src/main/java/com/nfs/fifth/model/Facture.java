package com.nfs.fifth.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="facture")
public class Facture implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id", nullable = false)
    private int id;

    private String validPaie;

    @Column(precision = 2, scale = 2, nullable = false)
    private double price;

    @Column(precision = 2, scale = 2, nullable = false)
    private double tva;

    @Column(precision = 2, scale = 2)
    private double tvaSomme;

    @Column(precision = 2, scale = 2)
    private double priceTTC;

    @Column(length=255, nullable=true)
    private String type;

    @Column(nullable = true)
    private LocalDate dateFacture;

    @Column(length=255, nullable=true)
    private String ref;

    @Column(length=255, nullable=false)
    private String nature;



    @ManyToOne
    @JoinColumn(name = "bilan_mois_id")
    @Autowired
    private BilanMois bilanmois;

    @OneToOne
    @JoinColumn(name = "formation_id", referencedColumnName = "id", nullable = false)
    private Formation formation;

    public Facture() {
    }

    public Facture(double price, double tva, String type, LocalDate dateFacture, String ref, String nature, BilanMois bilanmois, Formation formation) {
        this.price = price;
        this.tva = tva;
        this.type = type;
        this.dateFacture = dateFacture;
        this.ref = ref;
        this.nature = nature;
        this.bilanmois = bilanmois;
        this.formation = formation;
        this.tvaSomme = (this.price*tva)/100;
        this.priceTTC = this.price - this.tvaSomme;
        this.validPaie = "Payé";
    }

    public Facture(double price, double tva, LocalDate dateFacture, String nature, Formation formation, BilanMois bilanmois) {
        this.price = price;
        this.tva = tva;
        this.dateFacture = dateFacture;
        this.nature = nature;
        this.bilanmois = bilanmois;
        this.formation = formation;
        this.tvaSomme = (this.price*tva)/100;
        this.priceTTC = this.price - this.tvaSomme;
        this.validPaie = "En Attente de Payement";
    }

    public String getValidPaie() {
        return validPaie;
    }

    public void setValidPaie(String validPaie) {
        this.validPaie = validPaie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(LocalDate dateFacture) {
        this.dateFacture = dateFacture;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public BilanMois getBilanmois() {
        return bilanmois;
    }

    public void setBilanmois(BilanMois bilanmois) {
        this.bilanmois = bilanmois;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    public double getTvaSomme() {
        return tvaSomme;
    }

    public void setTvaSomme(double tvaSomme) {
        this.tvaSomme = tvaSomme;
    }

    public double getPriceTTC() {
        return priceTTC;
    }

    public void setPriceTTC(double priceTTC) {
        this.priceTTC = priceTTC;
    }
}
