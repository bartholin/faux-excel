package com.nfs.fifth.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="formation")
public class Formation implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id", nullable = false)
    private int id;

    @Column(name="action", length=255, nullable=false)
    private String action;

    @Column(nullable = false)
    private int nbCandidat;

    @Column(nullable = false)
    private LocalDate dateFormation;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    @Autowired
    private Client client;

    @OneToOne(mappedBy = "formation")
    private Facture facture;

    public Formation() {
    }

    public Formation(String action, int nbCandidat, LocalDate dateFormation, Client client) {
        this.action = action;
        this.nbCandidat = nbCandidat;
        this.dateFormation = dateFormation;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getNbCandidat() {
        return nbCandidat;
    }

    public void setNbCandidat(int nbCandidat) {
        this.nbCandidat = nbCandidat;
    }

    public LocalDate getDateFormation() {
        return dateFormation;
    }

    public void setDateFormation(LocalDate dateFormation) {
        this.dateFormation = dateFormation;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }


}
