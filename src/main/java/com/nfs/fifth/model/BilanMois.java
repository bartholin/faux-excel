package com.nfs.fifth.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="bilanmois")
public class BilanMois {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="bilan_mois_id", nullable = false)
    private int id;

    @Column(nullable = false)
    private LocalDate dateBilan;

    @OneToMany(mappedBy = "id")
    @Autowired
    private Set<Facture> facturesEnsemble;

    public BilanMois() {
    }

    public BilanMois(LocalDate dateBilan) {
        this.dateBilan = dateBilan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDateBilan() {
        return dateBilan;
    }

    public void setDateBilan(LocalDate dateBilan) {
        this.dateBilan = dateBilan;
    }

    public Set<Facture> getFactures() {
        return facturesEnsemble;
    }

    public void setFactures(Set<Facture> factures) {
        this.facturesEnsemble = factures;
    }

    public Set<Facture> getFacturesEnsemble() {
        return facturesEnsemble;
    }

    public void setFacturesEnsemble(Set<Facture> facturesEnsemble) {
        this.facturesEnsemble = facturesEnsemble;
    }

    public void addFacture(Facture facture){
        this.facturesEnsemble.add(facture);
    }

}
