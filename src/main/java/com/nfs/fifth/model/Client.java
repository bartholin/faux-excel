package com.nfs.fifth.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="client")
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="client_id", nullable = false)
    private int id;

    @Column(length=255, nullable=false)
    private String name;

    @OneToMany(mappedBy = "id")
    @Autowired
    private Set<Formation> formationClient;

    public Client() {
    }

    public Client(String name, Set<Formation> formationClient) {
        this.name = name;
        this.formationClient = formationClient;
    }

    public Client(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Formation> getFormationClient() {
        return formationClient;
    }

    public void setFormationClient(Set<Formation> formationClient) {
        this.formationClient = formationClient;
    }

}
