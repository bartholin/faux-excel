package com.nfs.fifth.repository;

import com.nfs.fifth.model.BilanMois;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;

@Repository
public interface BilanMoisRepository extends JpaRepository<BilanMois, Integer> {

    @Query("SELECT u FROM BilanMois u WHERE u.dateBilan = :dateBilan")
    BilanMois findByDate(@Param("dateBilan") LocalDate date);
}
