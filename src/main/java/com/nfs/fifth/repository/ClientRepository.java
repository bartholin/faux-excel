package com.nfs.fifth.repository;

import com.nfs.fifth.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

    @Query("SELECT u FROM Client u WHERE u.name = :name")
    Client findByName(String name);
}
