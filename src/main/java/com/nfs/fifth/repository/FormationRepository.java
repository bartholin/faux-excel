package com.nfs.fifth.repository;

import com.nfs.fifth.model.BilanMois;
import com.nfs.fifth.model.Formation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface FormationRepository extends JpaRepository<Formation, Integer> {

    @Query("SELECT u FROM Formation u WHERE u.id = :id")
    Formation findById(int id);
}
